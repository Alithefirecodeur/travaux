<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller;

class UploaderHelper 
{
    private $uploadsPath;
    public function __construct(string $uploadsPath)
    {
        $this->uploadsPath = $uploadsPath;
    }
   public function uploaderArticleImage(UploadedFile $uploadedFile)
   {
    $destination = $this->uploadsPath .'/images';
    $originalFilename = pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_FILENAME);
    $newFilename = $originalFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();
    $uploadedFile->move(
        $destination,
        $newFilename,
    ); 
    return $newFilename;
   } 
}