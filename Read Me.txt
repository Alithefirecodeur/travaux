Résumé du projet
Mon projet consiste en la conception et le développement d’un site internet 
Ce site permet de chercher :
• Les bars
• Les restaurants
• Les événements sur la région de Brest et ses alentours
• Des boutiques de vêtements et bijoux
• Des créateurs qui n’ont pas de local physique
• Un formulaire de contact
• Un Carrousel pour les nouveautés ajouté sur le site
Ça sera en forme de catégories qui seront les suivantes :
• A propos : petite description de la personne qui gère le site
• Restaurant : tous les restaurants avec qui la personne est partenaire
• Bars : tous les bars avec qui la personne est partenaire
• Coffee shop : pour les magasins de café spécialisé
• Magasins : tous les magasins de vêtements et bijoux
• Créateurs locaux : tous les créateurs de la région de Brest qui n’ont pas de
local physique
• Évènements : tous les événements se passant sur la région
• Contact : pour contacter l’administrateur pour des partenariats ou autres.
L’interface vue par le visiteur proposera un carrousel, les nouveautés publiées par l’administrateur.
Le site aura une interface administrateur qui consistera à gérer tout le site, c’est-à-dire, modifier, supprimer et ajouter des articles, qui sont dans la barre de navigation et aussi pour les nouveautés dans un carrousel qui mettra en avant les nouveaux magasins ou événements.
Pour développer tout cela, j’ai utilisé les technologies suivantes :
• Front-end : HTML 5, CSS3, JavaScript, Bootstrap.
• Back-end : Laravel, Php 7, SQL.
• Autres : Mailtrap, Git/Gitlab, Figma,jMerise,Stripe.